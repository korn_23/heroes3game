#pragma once
#include"GameState.h"
#include"Point.h"
#include<vector>
#include"ResourceManager.h"
#include "obstacleTypes.h"
#include"landscapeTypes.h"
#include "astar.h"
#include "MapGenerationMethods.h"
#include "Player.h"
#include "Tower.h"
#include "UserInterface.h"
#include "Constants.h"


//camX � camY ���������� � ������� 

class MainGameState :public GameState {
private:	
	Player *hero;
	Player *enemy;
	Player *selectedUnit;

	Tower **towers;
	sf::RectangleShape* tileShape;
	sf::RectangleShape *fogShape;
	sf::RectangleShape* obstacleShape;
	sf::CircleShape  *oneCircleOfPath = new sf::CircleShape();
	UserInterface *ui;
	OBSTACLE_TYPE t = OBSTACLE_TYPE::ROCK;
	int **fieldData;
	int **obstacleData;
	bool **fogData;
	int camX = 0, camY = 0;
	int countOfCellsPassed = -1;
	bool selectedUnitMustMove = false;
	astar::World *world = nullptr;
	std::vector<astar::Node*> path;
	float animTimer = 0;

	
	
	void setupFieldData() {
		fieldData = new int*[mapSize];
		for (int i = 0; i < mapSize; i++) {
			fieldData[i] = new int[mapSize];
		}
	}

	void setupFogData() {
		fogData = new bool*[mapSize];
		for (int i = 0; i < mapSize; i++) {
			fogData[i] = new bool[mapSize];
		}
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				fogData[i][j] = true;
			}
		}
	}
	void setupObstacleData() {
		obstacleData = new int*[mapSize];
		for (int i = 0; i < mapSize; i++) {
			obstacleData[i] = new int[mapSize];
		}
	}
	void setupTileShape() {
		tileShape = new sf::RectangleShape();
		tileShape->setSize(sf::Vector2f(cellSize, cellSize));
		tileShape->setOutlineThickness(0.0);
	}

	void setupFogShape() {
		fogShape = new sf::RectangleShape();
		fogShape->setSize(sf::Vector2f(cellSize, cellSize));
		fogShape->setOutlineThickness(0.0);
		fogShape->setFillColor(sf::Color::Black);
}
	void setupObstacleShape() {
		obstacleShape = new sf::RectangleShape();
		obstacleShape->setSize(sf::Vector2f(cellSize, cellSize));
		obstacleShape->setOutlineThickness(0.0);
	}
	void setupOneGreenCircleOfPath() {
		oneCircleOfPath->setRadius(6);
		oneCircleOfPath->setOutlineThickness(0.0);
		oneCircleOfPath->setFillColor(sf::Color::Green);
	}
	void setupHero() {
		hero = new Player((char*)"Sergey", cellSize, ResourceManager::sharedInstance().getTextureById(TEX_HERO));
	}
	void setupEnemy() {
		enemy = new Player((char*)"Enemy", cellSize, ResourceManager::sharedInstance().getTextureById(TEX_ENEMY));
		enemy->x = 30;
		enemy->y = 30;
		enemy->unit->setPosition(sf::Vector2f(enemy->x*cellSize, enemy->y*cellSize));
	}
	
	void setupTowers() {
		towers = new Tower*[5];
		towers[0] = new Tower((char*)("TowerLeftDown"), ResourceManager::sharedInstance().getTextureById(TEX_TOWER));
		towers[0]->x = 9;
		towers[0]->y = 35;
		towers[0]->unit->setPosition(sf::Vector2f(towers[0]->x*cellSize, towers[0]->y*cellSize));

		towers[1] = new Tower((char*)("TowerLeftTop"), ResourceManager::sharedInstance().getTextureById(TEX_TOWER));
		towers[1]->x = 9;
		towers[1]->y = 5;
		towers[1]->unit->setPosition(sf::Vector2f(towers[1]->x*cellSize, towers[1]->y*cellSize));

		towers[2] = new Tower((char*)("TowerMiddle"), ResourceManager::sharedInstance().getTextureById(TEX_TOWER));
		
		towers[3] = new Tower((char*)("TowerRightTop"), ResourceManager::sharedInstance().getTextureById(TEX_TOWER));
		towers[3]->x = 40;
		towers[3]->y = 5;
		towers[3]->unit->setPosition(sf::Vector2f(towers[3]->x*cellSize, towers[3]->y*cellSize));

		towers[4] = new Tower((char*)("TowerRightDown"), ResourceManager::sharedInstance().getTextureById(TEX_TOWER));
		towers[4]->x = 40;
		towers[4]->y = 35;
		towers[4]->unit->setPosition(sf::Vector2f(towers[1]->x*cellSize, towers[1]->y*cellSize));
	}

	void setupTowerAsObstacle(astar::World *world, Tower *tower , int i,int j) {
		if (i >= (tower->unit->getPosition().x / cellSize) &&
			i < ((tower->unit->getPosition().x / cellSize) + towerSizeX / cellSize) &&
			j >= (tower->unit->getPosition().y / cellSize) &&
			j < ((tower->unit->getPosition().y / cellSize) + towerSizeY / cellSize)) {
			world->setObstacle(i, j);
		}
	}

	void mapGeneration(int temp_late) {
		if (world != nullptr) {
			delete world;
		}

		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				obstacleData[i][j] = OBSTACLE_TYPE::NONE;
			}
		}

		world = new astar::World(mapSize, mapSize);
		std::vector<Point> points;
		//generate water
		if (temp_late == 1)MapGenerationMethods::generateWaterForIslandMiddle(points, mapSize);
		if (temp_late == 2)MapGenerationMethods::generateWaterForIslandDiagonal(points, mapSize);
		if (temp_late == 3)MapGenerationMethods::generateWaterForIslandInverseDiagonal(points, mapSize);
		//generation land points
		MapGenerationMethods::generateRandomLandPoints(points, mapSize);
		//calculating other cells
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				double minDistance = 10000;
				Point temp;
				for (int c = 0; c < points.size(); c++) {
					double dist = sqrt(abs(pow(points[c].x - i, 2)) + abs(pow(points[c].y - j, 2)));
					if (dist < minDistance) {
						minDistance = dist;
						temp = points[c];
					}
				}
				fieldData[i][j] = temp.landscape;
			}
		}
		//generate Obstacles
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				//for water
				if (fieldData[i][j] == (int)LANDSCAPE_TYPE::WATER && rand() % chanceOfPlaceObstacle == 0) {
					obstacleData[i][j] = (int)OBSTACLE_TYPE::LILY;
				}
				//for land and sand
				if ((fieldData[i][j] == (int)LANDSCAPE_TYPE::LAND ||
					fieldData[i][j] == (int)LANDSCAPE_TYPE::SAND) &&
					rand() % chanceOfPlaceObstacle == 0) {
					obstacleData[i][j] = (int)OBSTACLE_TYPE::ROCK;
				}
			}
		}
		//Setup pathfinding world
		for (int i = 0; i < mapSize; i++) {
			for (int j = 0; j < mapSize; j++) {
				world->setTileMovementCost(i, j, 10 + fieldData[i][j] * 10);
				//tower
				setupTowerAsObstacle(world, towers[0], i, j);
				setupTowerAsObstacle(world, towers[1], i, j);
				setupTowerAsObstacle(world, towers[2], i, j);
				setupTowerAsObstacle(world, towers[3], i, j);
				setupTowerAsObstacle(world, towers[4], i, j);

				if (fieldData[i][j] == 2) {
					world->setObstacle(i, j);
				}
				if (obstacleData[i][j] != OBSTACLE_TYPE::NONE) {
					world->setObstacle(i, j);
				}
			}
		}
	}

	void cameraMoving(int x,int y ) {
			if (x < CAMERA_MARGIN)
				camX--;
			if (x > WINDOW_WIDTH - CAMERA_MARGIN)
				camX++;
			if (y < CAMERA_MARGIN)
				camY--;
			if (y > WINDOW_HEIGHT - CAMERA_MARGIN)
				camY++;

			if (camX < 0)
				camX = 0;
			if (camX >= mapSize - ceil(WINDOW_WIDTH / cellSize))
				camX = mapSize - ceil(WINDOW_WIDTH / cellSize);
			if (camY < 0)
				camY = 0;
			if (camY >= mapSize - ceil(WINDOW_HEIGHT / cellSize))
				camY = mapSize - ceil(WINDOW_HEIGHT / cellSize);
		
	}

	void findPath(int selectedUnitX, int selectedUnitY, int mouseX, int mouseY) {
		path = world->findPath(selectedUnitX, selectedUnitY, mouseX, mouseY, astar::HeuristicFunction::EUCLIDEAN, true);


			countOfCellsPassed = path.size() - 1;
	
	}

	void pathBuilding(int selectedUnitX, int selectedUnitY, int mouseX, int mouseY) {

		if ((fieldData[mouseX][mouseY] == 0 ||
			fieldData[mouseX][mouseY] == 1) &&
			obstacleData[mouseX][mouseY] != OBSTACLE_TYPE::ROCK &&
			!(mouseX == selectedUnitX && mouseY == selectedUnitY)) {

			if (path.size() > 0) {
				//check that selectedUnit must move 
				auto node = *path.begin();
				if (mouseX == node->x && mouseY == node->y) {
					std::cout << "Start moving\n";
					selectedUnitMustMove = true;
				}
				else {
							findPath(selectedUnitX, selectedUnitY, mouseX, mouseY);
				}
			}
			else {
					findPath(selectedUnitX, selectedUnitY, mouseX, mouseY);
			}
		}
	}
		
	void towerFrameUpdate(int x , int y ,Tower *tower) {
		/*std::cout << tower->x << " " << tower->y << " " << selectedUnit->x << " " << selectedUnit->y << "\n";*/

		if (x == tower->x &&
			y == tower->y + towerSizeY / cellSize) {

			if (selectedUnit == hero) {
				if (tower->owner != 0) {
					if (tower->owner == 2) {
						hero->towersCaptured++;
						enemy->towersCaptured--;
						tower->owner = 1;
						tower->frameOfTower->setOutlineColor(sf::Color::Green);
					}
				}

				else {
					hero->towersCaptured++;
					tower->owner = 1;
					tower->frameOfTower->setOutlineColor(sf::Color::Green);
				}
			}
			if (selectedUnit == enemy) {
				if (tower->owner != 0) {
					if (tower->owner == 1) {
						enemy->towersCaptured++;
						hero->towersCaptured--;
						tower->owner = 2;
						tower->frameOfTower->setOutlineColor(sf::Color::Red);
					}
				}
					else {
						enemy->towersCaptured++;
						tower->owner = 2;
						tower->frameOfTower->setOutlineColor(sf::Color::Red);
					}
					
			}
		}
	}
	
	void fogUpdate() {
		for (int i = (selectedUnit->x - selectedUnit->radiusOfView); i < (selectedUnit->x + selectedUnit->radiusOfView); i++) {
			for (int j = (selectedUnit->y - selectedUnit->radiusOfView); j < (selectedUnit->y + selectedUnit->radiusOfView); j++) {
				fogData[i][j] = false;
			}
		}
	}

	void selectedUnitMove() {
		if (animTimer > 0.1) {
			
			if (path.size() > 0 && selectedUnitMustMove && selectedUnit->MP>0) {
				if (countOfCellsPassed == 0) {
					path.clear();
					animTimer = 0;
					selectedUnitMustMove = false;
				}
				else {
					towerFrameUpdate(path[countOfCellsPassed - 1]->x, path[countOfCellsPassed - 1]->y,towers[0]);
					towerFrameUpdate(path[countOfCellsPassed - 1]->x, path[countOfCellsPassed - 1]->y, towers[1]);
					towerFrameUpdate(path[countOfCellsPassed - 1]->x, path[countOfCellsPassed - 1]->y, towers[2]);
					towerFrameUpdate(path[countOfCellsPassed - 1]->x, path[countOfCellsPassed - 1]->y, towers[3]);
					towerFrameUpdate(path[countOfCellsPassed - 1]->x, path[countOfCellsPassed - 1]->y, towers[4]);

					std::cout << "moving to " << path[countOfCellsPassed]->x << "  " << path[countOfCellsPassed]->y << std::endl;
					selectedUnit->x = path[countOfCellsPassed - 1]->x;
					selectedUnit->y = path[countOfCellsPassed - 1]->y;
					selectedUnit->MP -= path[countOfCellsPassed - 1]->getCost() / 10;
					countOfCellsPassed--;
					animTimer = 0;
				}
			}
			else animTimer = 0;
		}
	}
		
	
	void drawTileShape(int i,int j, sf::RenderWindow *window) {
		tileShape->setPosition(sf::Vector2f((i - camX) * cellSize, (j - camY) * cellSize));

		if (fieldData[i][j] == 0) {
			tileShape->setTexture(ResourceManager::sharedInstance().getTextureById(TEX_LAND));
		}
		if (fieldData[i][j] == 1) {
			tileShape->setTexture(ResourceManager::sharedInstance().getTextureById(TEX_SAND));
		}
		if (fieldData[i][j] == 2) {
			tileShape->setTexture(ResourceManager::sharedInstance().getTextureById(TEX_WATER));
		}
		window->draw(*tileShape);
	}

	void drawFogShape(int i, int j, sf::RenderWindow *window) {
		fogShape->setPosition(sf::Vector2f((i - camX) * cellSize, (j - camY) * cellSize));
		if (fogData[i][j])window->draw(*fogShape);
	}
	void drawObstacleShape(int i, int j, sf::RenderWindow *window) {
		if (obstacleData[i][j] != OBSTACLE_TYPE::NONE) {
			obstacleShape->setPosition(sf::Vector2f((i - camX) * cellSize, (j - camY) * cellSize));

			if (obstacleData[i][j] == OBSTACLE_TYPE::LILY) {
				obstacleShape->setTexture(ResourceManager::sharedInstance().getTextureById(TEX_LILY));
			}

			if (obstacleData[i][j] == OBSTACLE_TYPE::ROCK) {
				obstacleShape->setTexture(ResourceManager::sharedInstance().getTextureById(TEX_STONE));
			}

			window->draw(*obstacleShape);
		}
	}
	void drawPath(sf::RenderWindow *window) {
		if (path.size() > 0) {

			float temp = 0;
			for (int i = countOfCellsPassed - 1;i>-1 ; i--) {

				if (temp < selectedUnit->MP) oneCircleOfPath->setFillColor(sf::Color::Green);
				else oneCircleOfPath->setFillColor(sf::Color::Red);
				
				oneCircleOfPath->setPosition(sf::Vector2f((-camX) * cellSize + path[i]->x*cellSize + 12, (-camY) * cellSize + path[i]->y*cellSize + 12));
				if (i != path.size() - 1) {
					std::cout << "temp before plus " << temp << "\n\n";
					temp += path[i]->getCost() / 10;
					std::cout << "temp after plus " << temp << "\n\n";
				}
				window->draw(*oneCircleOfPath);
			}
		}
	}

	void drawHero(sf::RenderWindow *window){
		window->draw(*hero->unit);
	}

	void drawEnemy(sf::RenderWindow *window) {
		window->draw(*enemy->unit);
	}
	void drawTowers(sf::RenderWindow *window) {
		for (int i = 0; i < 5; i++) {
			towers[i]->draw(window);
		}
		
	}

public:
	
	~MainGameState() {
		delete tileShape;
		delete world;

		for (int i = 0; i < mapSize; i++) {
			delete[] fieldData[i];
		}
		delete[] fieldData;

		for (int i = 0; i < mapSize; i++) {
			delete[] obstacleData[i];
		}
		delete[] obstacleData;
		delete ui;
	}
	
	
	void init(sf::RenderWindow *window) {
		setupFieldData();
		setupFogData();
		setupObstacleData();	
		setupTileShape();
		setupObstacleShape();
		setupFogShape();
		setupOneGreenCircleOfPath();
		setupHero();
		setupEnemy();
		setupTowers();
		selectedUnit = hero;
		ui = new UserInterface();
		ui->setSelectedUnit(selectedUnit);

		mapGeneration(1);
	}

	
	void handleEvent(sf::Event& e, sf::RenderWindow *window) {
		auto eventData = ui->handleEvent(e);
		if (eventData.result == END_TURN) {
			path.clear();
			if (selectedUnit == hero) {
				selectedUnit = enemy;
				ui->setSelectedUnit(enemy);
			}
			else {
				selectedUnit = hero;
				ui->setSelectedUnit(hero);
			}
			selectedUnit->MP = selectedUnit->MP_MAX;
			selectedUnitMustMove = false;
			
		}

		if (eventData.result != UNDEFINED) {
			return;
		}

		if (e.type == sf::Event::MouseButtonReleased) {
			if (e.mouseButton.button == sf::Mouse::Button::Right) {

				int mouseX = e.mouseButton.x / cellSize + camX;
				int mouseY = e.mouseButton.y / cellSize + camY;
				int selectedUnitX = (selectedUnit->x);
				int selectedUnitY = (selectedUnit->y);
				
				pathBuilding(selectedUnitX, selectedUnitY,mouseX,mouseY);
			}
		}	else if (e.type == sf::Event::MouseMoved) {
			cameraMoving(e.mouseMove.x  ,  e.mouseMove.y );
		}

		
	}
	
	
	void update(float dt) {
		
		fogUpdate();
		animTimer += dt;
		selectedUnitMove();
		

		ui->update(dt,hero,enemy);
	}
	
	void draw(sf::RenderWindow *window) {
		for (int i = camX; i < camX + ceil(window->getSize().x / cellSize); i++) {
			for (int j = camY; j < camY + ceil(window->getSize().y / cellSize); j++) {

				drawTileShape(i, j, window);
				drawObstacleShape(i,j,window);
			}
		}

		hero->unit->setPosition(sf::Vector2f((-camX) * cellSize + hero->x * cellSize, (-camY) * cellSize + hero->y * cellSize));

		enemy->unit->setPosition(sf::Vector2f((-camX) * cellSize + enemy->x * cellSize, (-camY) * cellSize + enemy->y * cellSize));

		towers[0]->unit->setPosition(sf::Vector2f((-camX) * cellSize + towers[0]->x* cellSize, (-camY) * cellSize + towers[0]->y*cellSize));
		towers[0]->frameOfTower->setPosition(sf::Vector2f((-camX) * cellSize + towers[0]->x*cellSize, (-camY) * cellSize + towers[0]->y*cellSize + cellSize * 2));

		towers[1]->unit->setPosition(sf::Vector2f((-camX) * cellSize + towers[1]->x* cellSize, (-camY) * cellSize + towers[1]->y*cellSize));
		towers[1]->frameOfTower->setPosition(sf::Vector2f((-camX) * cellSize + towers[1]->x*cellSize, (-camY) * cellSize + towers[1]->y*cellSize+ cellSize*2));
		
		towers[2]->unit->setPosition(sf::Vector2f((-camX) * cellSize + towers[2]->x* cellSize, (-camY) * cellSize + towers[2]->y*cellSize));
		towers[2]->frameOfTower->setPosition(sf::Vector2f((-camX) * cellSize + towers[2]->x*cellSize, (-camY) * cellSize + towers[2]->y*cellSize + cellSize * 2));

		towers[3]->unit->setPosition(sf::Vector2f((-camX) * cellSize + towers[3]->x* cellSize, (-camY) * cellSize + towers[3]->y*cellSize));
		towers[3]->frameOfTower->setPosition(sf::Vector2f((-camX) * cellSize + towers[3]->x*cellSize, (-camY) * cellSize + towers[3]->y*cellSize + cellSize * 2));

		towers[4]->unit->setPosition(sf::Vector2f((-camX) * cellSize + towers[4]->x* cellSize, (-camY) * cellSize + towers[4]->y*cellSize));
		towers[4]->frameOfTower->setPosition(sf::Vector2f((-camX) * cellSize + towers[4]->x*cellSize, (-camY) * cellSize + towers[4]->y*cellSize + cellSize * 2));
		
		drawPath(window);
		drawHero(window);
		drawEnemy(window);
		drawTowers(window);
		



		for (int i = camX; i < camX + ceil(window->getSize().x / cellSize); i++) {
			for (int j = camY; j < camY + ceil(window->getSize().y / cellSize); j++) {
				drawFogShape(i, j, window);
			}
		}
		ui->draw(window);
	}
};
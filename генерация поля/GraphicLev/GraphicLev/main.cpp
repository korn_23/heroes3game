#include<iostream> 
#include <SFML/Graphics.hpp> 
#include <SFML/Audio.hpp>
#include<stdlib.h> 
#include<time.h> 
#include <sstream> 
#include"MainGameState.h"
#include"Constants.h"
GameState *activeGameState;

int main()
{
	srand(time(NULL));
	
	sf::Clock timer;
	sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Game");//�������� ���� � ����������� 
	activeGameState = new MainGameState ();
	activeGameState->init(window);

	while (window->isOpen())
	{
		sf::Event evt;
		
		while (window->pollEvent(evt))
		{
			if (evt.type == sf::Event::Closed)
			{
				window->close();
			}
			activeGameState->handleEvent(evt, window);
			
		}
		
		activeGameState->update(timer.restart().asSeconds());
		
		window->clear(sf::Color::Black);
		
		activeGameState->draw(window);

		window->display();
	}
	return 0;
}
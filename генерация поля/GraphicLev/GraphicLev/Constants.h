#pragma once

#define WINDOW_WIDTH 960
#define WINDOW_HEIGHT 960
#define mapSize 50
#define cellSize 32
#define obstacleSize 3
#define chanceOfPlaceObstacle 40

#define END_TURN_BUTTON_WIDTH 200
#define END_TURN_BUTTON_HEIGHT 50

#define CAMERA_MARGIN 50

#define towerSizeX cellSize*2
#define towerSizeY cellSize*4
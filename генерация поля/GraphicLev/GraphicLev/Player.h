#pragma once
#include"Unit.h"
#include"ResourceManager.h"

class Player: public Unit {
private:
	
public:
	Player(char *name, int cellSize ,sf::Texture *tex ) {
		radiusOfView = 10;
		towersCaptured = 0;
		HP_MAX = 100;
		HP = 100;
		MP_MAX = 10;
		MP = 10;
		x = 20, y = 20;
		this->name = name;
		unit = new sf::RectangleShape();
		unit->setSize(sf::Vector2f(cellSize, cellSize));
		unit->setPosition(sf::Vector2f(x * cellSize , y * cellSize));
		unit->setOutlineThickness(0.0);
		unit->setTexture(tex);
	}
	
	void move(sf::Event &e,int **fieldData ) {
		;
	}
};
#pragma once
#include<iostream>
#include "Unit.h"
#include "SFML/Graphics.hpp"
#include "ResourceManager.h"
#include<string>
#include "Constants.h"
#include "Player.h"
#include "Unit.h"
enum UI_EVENT_RESULT {
	UNDEFINED,
	END_TURN
};

struct UI_EVENT_DATA {
	UI_EVENT_RESULT result;
	void* data;

	UI_EVENT_DATA(UI_EVENT_RESULT result, void* data) {
		this->result = result;
		this->data = data;
	}
};

class UserInterface {
private:
	sf::RectangleShape* endTurnButton;
	sf::Text* endTurnButtonText;
	sf::Text* heroMPtext;
	sf::Text *heroHPtext;
	sf::Text *towersCapturedText;
	sf::Text *heroTowersCapturedText;
	sf::Text *enemyTowersCapturedText;
	sf::Font *font;
	
	Unit* selectedUnit = nullptr;


	void setupFont() {
		font = new sf::Font();
		font->loadFromFile("gameFont.ttf");
	}
	void setupHeroMPtext() {
		heroMPtext = new sf::Text();
		heroMPtext->setFont(*font);
		heroMPtext->setCharacterSize(35);
		heroMPtext->setFillColor(sf::Color::Yellow);
		heroMPtext->setOutlineThickness(2.0);
		heroMPtext->setPosition(sf::Vector2f(0, WINDOW_HEIGHT - heroMPtext->getCharacterSize()));
	}
	void setupHeroHPtext() {
		heroHPtext = new sf::Text();
		heroHPtext->setFont(*font);
		heroHPtext->setCharacterSize(35);
		heroHPtext->setFillColor(sf::Color::Yellow);
		heroHPtext->setOutlineThickness(2.0);
		heroHPtext->setPosition(sf::Vector2f(150, heroMPtext->getPosition().y));
	}
	void setupTowersCapturedText() {
		towersCapturedText = new sf::Text();
		towersCapturedText->setFont(*font);
		towersCapturedText->setCharacterSize(35);
		towersCapturedText->setFillColor(sf::Color::Yellow);
		towersCapturedText->setOutlineThickness(2.0);
		towersCapturedText->setPosition(sf::Vector2f(0, 0));
		towersCapturedText->setString("Towers Captured :");
	}
	void setupHeroTowersCapturedText() {
		heroTowersCapturedText = new sf::Text();
		heroTowersCapturedText->setFont(*font);
		heroTowersCapturedText->setCharacterSize(35);
		heroTowersCapturedText->setFillColor(sf::Color::Yellow);
		heroTowersCapturedText->setOutlineThickness(2.0);
		heroTowersCapturedText->setPosition(sf::Vector2f(0, 0 + towersCapturedText->getCharacterSize()));
		heroTowersCapturedText->setString("hero :0");
	}
	void setupEnemyTowersCapturedText() {
		enemyTowersCapturedText = new sf::Text();
		enemyTowersCapturedText->setFont(*font);
		enemyTowersCapturedText->setCharacterSize(35);
		enemyTowersCapturedText->setFillColor(sf::Color::Yellow);
		enemyTowersCapturedText->setOutlineThickness(2.0);
		enemyTowersCapturedText->setPosition(sf::Vector2f(0, 0 + towersCapturedText->getCharacterSize()+heroTowersCapturedText->getCharacterSize()));
		enemyTowersCapturedText->setString("enemy :0");
	}
	void setupEndTurnButton() {
		endTurnButton = new sf::RectangleShape();
		endTurnButton->setSize(sf::Vector2f(END_TURN_BUTTON_WIDTH, END_TURN_BUTTON_HEIGHT));
		endTurnButton->setPosition(sf::Vector2f(WINDOW_WIDTH - END_TURN_BUTTON_WIDTH - CAMERA_MARGIN, WINDOW_HEIGHT - END_TURN_BUTTON_HEIGHT - CAMERA_MARGIN));
		endTurnButton->setFillColor(sf::Color::Blue);
	}
	void setupEndTurnButtonText() {
		endTurnButtonText = new sf::Text();
		endTurnButtonText->setFont(*font);
		endTurnButtonText->setCharacterSize(30);
		endTurnButtonText->setFillColor(sf::Color::Yellow);
		endTurnButtonText->setOutlineThickness(2.0);
		endTurnButtonText->setPosition(endTurnButton->getPosition().x + 10, endTurnButton->getPosition().y + 10);
		endTurnButtonText->setString("END TURN");
	}

public:
	UserInterface() {
		
		setupFont();
		setupHeroMPtext();
		setupHeroHPtext();
		setupTowersCapturedText();
		setupHeroTowersCapturedText();
		setupEnemyTowersCapturedText();
		setupEndTurnButton();
		setupEndTurnButtonText();
	}

	~UserInterface() {}

	UI_EVENT_DATA handleEvent(sf::Event& e) {
		
		if (e.type == sf::Event::MouseButtonReleased) {
			if (e.mouseButton.button == sf::Mouse::Button::Left) {
				if (e.mouseButton.x > endTurnButton->getPosition().x &&
					e.mouseButton.x <(endTurnButton->getPosition().x + endTurnButton->getSize().x) &&
					e.mouseButton.y >endTurnButton->getPosition().y &&
					e.mouseButton.x < (endTurnButton->getPosition().y + endTurnButton->getSize().y)) {
					
					return UI_EVENT_DATA(UI_EVENT_RESULT::END_TURN, nullptr);
				}
				
			}
		}

		return UI_EVENT_DATA(UI_EVENT_RESULT::UNDEFINED, nullptr);
	}

	void update(float dt , Unit *hero , Unit *enemy) {
		heroMPtext->setString("MP:" + std::to_string(selectedUnit->MP));
		heroHPtext->setString("HP:" + std::to_string(selectedUnit->HP));
		heroTowersCapturedText->setString("hero :" + std::to_string(hero->towersCaptured));
		enemyTowersCapturedText->setString("enemy :" + std::to_string(enemy->towersCaptured));
		
	}

	void draw(sf::RenderWindow* window) {
		
		window->draw(*heroMPtext);
		window->draw(*heroHPtext);
		window->draw(*endTurnButton);
		window->draw(*endTurnButtonText);
		window->draw(*towersCapturedText); 
		window->draw(*heroTowersCapturedText);
		window->draw(*enemyTowersCapturedText);
	}
	
	
	void setSelectedUnit(Unit* unit) {
		selectedUnit = unit;
	}
	

};
#pragma once 
#include <SFML/Graphics.hpp>

class Unit {
private:
public:
	int radiusOfView;
	int x, y;
	int towersCaptured;
	int x_expected, y_expected;
	sf::RectangleShape *unit;
	int HP_MAX;
	int HP;
	int MP_MAX;      //��������� ����
	int MP;
	int owner = 0; //0 - neutral, 1 - player 1....
	char *name;
	void draw(sf::RenderWindow *window);
};
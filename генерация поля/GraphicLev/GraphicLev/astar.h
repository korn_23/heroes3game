#pragma once
#include <math.h>
#include <vector>
#include <functional>
#include <algorithm>
#include <SFML/Graphics.hpp>

#define __ASTAR_COST_OBSTACLE__ -1

namespace astar {
	enum HeuristicFunction {
		NONE,
		MANHATTAN,
		EUCLIDEAN
	};

	struct Node {
	private:
		float cost = 1;

	public:
		int x = -1, y = -1;
		float f = 0, g = 0, h = 0;
		Node* cameFrom = nullptr;

		Node() {
			;
		}

		Node(int x, int y) : Node() {
			this->x = x;
			this->y = y;
		}

		Node(int x, int y, int cost) : Node(x, y) {
			this->cost = cost;
		}

		friend bool operator< (const Node& lhs, const Node& rhs) {
			return lhs.f < rhs.f;
		}

		inline bool operator== (const Node& other) {
			return x == other.x && y == other.y;
		}

		float getCost() {
			return cost;
		}

		bool setCost(float cost) {
			if (cost < 0 && cost != __ASTAR_COST_OBSTACLE__) {
				return false;
			}

			this->cost = cost;
			return true;
		}

		bool isObstacle() {
			return cost == __ASTAR_COST_OBSTACLE__;
		}
	};

	class World {
	private:
		int width = 0, height = 0;
		Node ***nodes = nullptr;
		float heuristicMinFactor = INFINITY;
		float heuristicMaxFactor = -INFINITY;
		float heuristicFactor = 0;
		std::vector<Node*> openSet, closedSet, pathMap;

		void freeNodes() {
			if (nodes != nullptr) {
				for (int i = 0; i < width; i++) {
					for (int j = 0; j < height; j++) {
						delete[] nodes[i][j];
					}
					delete[] nodes[i];
				}
				delete[] nodes;
				nodes = nullptr;
			}
		}

		void freePathMap() {
			for (auto it = pathMap.begin(); it != pathMap.end(); ++it) {
				delete *it;
			}
			pathMap.clear();
		}

		void allocNodes(int width, int height) {
			freeNodes();

			this->width = width;
			this->height = height;

			nodes = new Node**[width];
			for (int i = 0; i < width; i++) {
				nodes[i] = new Node*[height];
				for (int j = 0; j < height; j++) {
					nodes[i][j] = new Node(i, j);
				}
			}
		}

		bool areCoordsValid(int x, int y) {
			return x >= 0 && y >= 0 && x < width && y < height;
		}

		std::vector<Node*> getNeighbors(Node* node, bool includeDiagonalNeighbors) {
			std::vector<Node*> neighbors;

			for (int i = node->x - 1; i <= node->x + 1; i++) {
				for (int j = node->y - 1; j <= node->y + 1; j++) {
					if (i == node->x && j == node->y)
						continue;

					if (!includeDiagonalNeighbors && abs(i - node->x) == 1 && abs(j - node->y) == 1)
						continue;

					if (areCoordsValid(i, j) && !nodes[i][j]->isObstacle())
						neighbors.push_back(nodes[i][j]);
				}
			}

			return neighbors;
		}

		float heuristicManhattan(Node* start, Node* goal) {
			return (abs(goal->x - start->x) + abs(goal->y - start->y)) * heuristicFactor;
		}

		float heuristicEuclidean(Node* start, Node* goal) {
			return (sqrt(pow(goal->x - start->x, 2) + pow(goal->y - start->y, 2))) * heuristicFactor;
		}

		std::vector<Node*> findPath(Node* start, Node* goal, HeuristicFunction heuristicFunction, bool allowDiagonalMovement) {
			std::function<float(Node*, Node*)> heuristicCostEstimate = [](Node* start, Node* goal) {
				return 0;
			};

			if (heuristicFunction == HeuristicFunction::MANHATTAN) {
				heuristicCostEstimate = [=](Node* start, Node* goal) {
					return this->heuristicManhattan(start, goal);
				};
			}
			else if (heuristicFunction == HeuristicFunction::EUCLIDEAN) {
				heuristicCostEstimate = [=](Node* start, Node* goal) {
					return this->heuristicEuclidean(start, goal);
				};
			}

			freePathMap();
			openSet.clear();
			closedSet.clear();

			start->cameFrom = nullptr;
			start->g = 0;
			start->h = heuristicCostEstimate(start, goal);
			start->f = start->g + start->h;

			openSet.push_back(start);

			while (!openSet.empty()) {
				std::sort(openSet.begin(), openSet.end(), [](Node* a, Node* b) -> bool {
					return *a < *b;
				});
				auto current = *openSet.begin();

				if (current == goal) {
					while (current != nullptr) {
						pathMap.push_back(new Node(*current));
						current = current->cameFrom;
					}

					return pathMap;
				}

				openSet.erase(openSet.begin());
				closedSet.push_back(current);

				auto neighbors = getNeighbors(current, allowDiagonalMovement);
				for (auto it = neighbors.begin(); it != neighbors.end(); ++it) {
					auto neighbor = *it;

					if (std::find(closedSet.begin(), closedSet.end(), neighbor) != closedSet.end())
						continue;

					float tentativeGScore = current->g + neighbor->getCost();
					bool tentativeIsBetter = false;

					if (std::find(openSet.begin(), openSet.end(), neighbor) == openSet.end()) {
						openSet.push_back(neighbor);
						tentativeIsBetter = true;
					}
					else {
						if (tentativeGScore < neighbor->g) {
							tentativeIsBetter = true;
						}
					}

					if (tentativeIsBetter) {
						neighbor->cameFrom = current;
						neighbor->g = tentativeGScore;
						neighbor->h = heuristicCostEstimate(neighbor, goal);
						neighbor->f = neighbor->g + neighbor->h;
					}
				}
			}

			return std::vector<Node*>();
		}

	public:
		World(int width, int height) {
			allocNodes(width, height);
		}

		~World() {
			freeNodes();
			freePathMap();
		}

		void setTileMovementCost(int x, int y, float cost) {
			if (!areCoordsValid(x, y))
				return;

			if (nodes[x][y]->setCost(cost)) {
				if (cost != __ASTAR_COST_OBSTACLE__) {
					if (cost < heuristicMinFactor)
						heuristicMinFactor = cost;
					if (cost > heuristicMaxFactor)
						heuristicMaxFactor = cost;

					heuristicFactor = heuristicMinFactor + (heuristicMaxFactor - heuristicMinFactor) / 2;
				}
			}
		}

		void setObstacle(int x, int y) {
			setTileMovementCost(x, y, __ASTAR_COST_OBSTACLE__);
		}

		std::vector<Node*> findPath(int fromX, int fromY, int toX, int toY, HeuristicFunction heuristicFunction, bool allowDiagonalMovement) {
			if (areCoordsValid(fromX, fromY) && areCoordsValid(toX, toY)) {
				auto start = nodes[fromX][fromY];
				auto goal = nodes[toX][toY];

				if (start->isObstacle() || goal->isObstacle())
					return std::vector<Node*>();

				return findPath(start, goal, heuristicFunction, allowDiagonalMovement);
			}
			return std::vector<Node*>();
		}

		std::vector<Node*> findPath(int fromX, int fromY, int toX, int toY, bool allowDiagonalMovement) {
			return findPath(fromX, fromY, toX, toY, HeuristicFunction::MANHATTAN, allowDiagonalMovement);
		}

		std::vector<Node*> _debug_getClosedSet() {
			return closedSet;
		}
	};
}
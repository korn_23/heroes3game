#pragma once
#include"Unit.h"
#include"ResourceManager.h"
#include "Constants.h"
class Tower : public Unit {
private:

public:
	sf::CircleShape *frameOfTower;

	Tower (char *name, sf::Texture *tex) {
		owner = 0;
		HP_MAX = 500;
		HP = 500;
		MP_MAX = 0;
		MP =0;
		this->name = name;
		unit = new sf::RectangleShape();
		unit->setSize(sf::Vector2f(towerSizeX, towerSizeY));
		x = (mapSize - unit->getSize().x/cellSize)/2 , y = (mapSize  - unit->getSize().y/cellSize) / 2;
		unit->setPosition(sf::Vector2f(x *cellSize, y*cellSize));
		unit->setOutlineThickness(0.0);
		unit->setTexture(tex);

		frameOfTower = new sf::CircleShape();
		frameOfTower->setRadius(cellSize);
		frameOfTower->setOutlineThickness(3.0);
		frameOfTower->setFillColor(sf::Color::Transparent);
		frameOfTower->setOutlineColor(sf::Color::White); 
		frameOfTower->setPosition(x*cellSize, y*cellSize+ cellSize * 2);

	}

	Tower() {};

	void draw(sf::RenderWindow *window) {
		window->draw(*frameOfTower);
		window->draw(*unit);
	}



	
};
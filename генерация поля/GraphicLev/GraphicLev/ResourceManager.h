#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>
#define TEX_COUNT 8
#define TEX_WATER 0
#define TEX_SAND 1
#define TEX_LAND 2
#define TEX_STONE 3
#define TEX_LILY 4
#define TEX_HERO 5
#define TEX_TOWER 6
#define TEX_ENEMY 7

class ResourceManager {
private:
	sf::Texture *textures[TEX_COUNT];

	ResourceManager() {
		for (int i = 0; i < TEX_COUNT; i++) {
			textures[i] = new sf::Texture();
		}


		//Load Textures
		textures[TEX_WATER]->loadFromFile("water.png");
		std::cout << "water loaded\n";
		textures[TEX_SAND]->loadFromFile("sand.png");
		std::cout << "sand loaded\n";
		textures[TEX_LAND]->loadFromFile("land.png");
		std::cout << "land loaded\n";
		textures[TEX_STONE]->loadFromFile("stone.png");
		std::cout << "stone loaded\n";
		textures[TEX_LILY]->loadFromFile("lily.png");
		std::cout << "lily loaded\n";
		textures[TEX_HERO]->loadFromFile("hero.png");
		std::cout << "hero loaded\n";
		textures[TEX_TOWER]->loadFromFile("tower.png");
		std::cout << "tower loaded\n";
		textures[TEX_ENEMY]->loadFromFile("enemy.png");
		std::cout << "enemy loaded\n";
		
	};

	~ResourceManager() {
		for (int i = 0; i < TEX_COUNT; i++) {
			delete textures[i];
		}
	}

public:
	static ResourceManager& sharedInstance() {
		static ResourceManager instance;
		return instance;
	}

	sf::Texture* getTextureById(int id) {
		if (id >= TEX_COUNT || id < 0) {
			return nullptr;
		}
		return textures[id];
	};
};

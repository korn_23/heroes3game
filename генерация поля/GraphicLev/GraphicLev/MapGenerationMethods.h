#pragma once
#include "Point.h"
#include <vector>
#include "ResourceManager.h"


class MapGenerationMethods {
public:
	//Water generation
	static void generateWaterForIslandMiddle(std::vector<Point> &points, int mapSize);
	static void generateWaterForIslandDiagonal(std::vector<Point> &points, int mapSize);
	static void generateWaterForIslandInverseDiagonal(std::vector<Point> &points, int mapSize);
	
	//Land generation
	static void generateRandomLandPoints(std::vector<Point> &points, int mapSize);
};

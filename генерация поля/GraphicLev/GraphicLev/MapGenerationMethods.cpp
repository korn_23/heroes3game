#include"MapGenerationMethods.h"

 void MapGenerationMethods::generateWaterForIslandMiddle(std::vector<Point> &points, int mapSize) {
	for (int i = 0; i < mapSize; i++) {
		for (int j = 0; j < mapSize; j++) {
			if (i == 0 || j == 0 || i == mapSize - 1 || j == mapSize - 1) {
				Point temp(i, j, 2);
				points.push_back(temp);
			}
		}
	}
}


void MapGenerationMethods::generateWaterForIslandDiagonal(std::vector<Point> &points, int mapSize) {
	 int temp = 1;
	 for (int i = 5; i < mapSize; i++) {
		 for (int j = 0; j < temp; j++) {
			 Point temp(i, j, 2);
			 points.push_back(temp);
		 }
		 temp++;
	 }
	 temp = 6;
	 for (int i = 0; i < mapSize - 5; i++) {
		 for (int j = mapSize; j > temp; j--) {
			 Point temp(i, j, 2);
			 points.push_back(temp);
		 }
		 temp++;
	 }
 }



  void MapGenerationMethods::generateWaterForIslandInverseDiagonal(std::vector<Point> &points, int mapSize) {
	 int temp = 1;
	 for (int i = mapSize - 5; i > 0; i--) {
		 for (int j = 0; j < temp; j++) {
			 Point temp(i, j, 2);
			 points.push_back(temp);
		 }
		 temp++;
	 }
	 temp = mapSize - 1;
	 for (int i = 5; i < mapSize; i++) {
		 for (int j = mapSize; j > temp; j--) {
			 Point temp(i, j, 2);
			 points.push_back(temp);
		 }
		 temp--;
	 }
 }


void MapGenerationMethods::generateRandomLandPoints(std::vector<Point> &points, int mapSize) {
	for (int i = 0; i < mapSize; i++){
		Point p(rand() % mapSize, rand() % mapSize, rand() % 2);
		points.push_back(p);
	}
  }




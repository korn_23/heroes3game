#pragma once
#include <SFML/Graphics.hpp>

class GameState {
public:
	void(*callback)(int);

	virtual void init(sf::RenderWindow *window) = 0;
	virtual void handleEvent(sf::Event& e, sf::RenderWindow *window) = 0;
	virtual void update(float dt) = 0;
	virtual void draw(sf::RenderWindow *window) = 0;
};
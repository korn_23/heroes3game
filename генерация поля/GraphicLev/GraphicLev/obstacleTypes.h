#pragma once

enum OBSTACLE_TYPE {
	NONE=-1,
	TREE=0,
	ROCK=1,
	LILY = 2
};